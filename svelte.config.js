import sveltePreprocess from "svelte-preprocess";

/**
 * This will add autocompletion if you're working with SvelteKit
 *
 * @type {import('@sveltejs/kit').Config}
 */
export default {
  preprocess: sveltePreprocess({
    scss: {
      prependData: `@import "./src/sass/variables.scss";`,
    },
  }),
};
