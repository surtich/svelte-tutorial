import { writable } from "svelte/store";

const defaultSettings = {
  colorSchema: "light",
  language: "en",
  fontSize: 12,
};

function createSettingsStore() {
  // Es importante desestructurar defaultSettings para que no resulte mutado en actualizaciones del store
  const { subscribe, set, update } = writable({ ...defaultSettings });

  return {
    subscribe,
    set,
    update,
    updateSetting: (setting, value) =>
      update((settings) => ({ ...settings, [setting]: value })),
    reset: () => {
      set({ ...defaultSettings });
    },
  };
}

export default createSettingsStore();
