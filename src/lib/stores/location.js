import { readable } from "svelte/store";

// 1 null porque no sabemos el valor inicial
// 2 La función de callback será llamada cuando se suscriba el primer observador.
//  Esta función también existe en los stores writable.
// 3 Retorna una función que se invocará cuando no haya más observadores suscritos.
const location = readable(
  /* 1 */ null,
  /* 2 */ (set) => {
    let watchId;
    if (navigator.geolocation && navigator.geolocation.watchPosition) {
      watchId = navigator.geolocation.watchPosition(
        (position) => {
          const { latitude, longitude } = position.coords;
          set({ latitude, longitude });
        },
        (error) => {
          set({ error });
        }
      );
    }

    /* 3 */ return () => {
      if (navigator.geolocation && navigator.geolocation.clearWatch) {
        navigator.geolocation.clearWatch(watchId);
      }
      set(null);
    };
  }
);

export default location;
