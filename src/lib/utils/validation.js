export const validateFieldRequired = (value, label = "Field") => {
  let error;
  if (!value) {
    error = `The ${label} is required.`;
  }
  return error;
};

export const validateEmail = (value, label = "Field") => {
  let error;
  if (!/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/.test(value)) {
    error = `The ${label} is a invalid email.`;
  }
  return error;
};
